/*
 * This hash table is intended to be used by first converting anything you store into
 * it to a string.
 */

// declares structure in which things are stored in hash table
typedef struct _node {
	// stores hashcode for faster rehashing
	unsigned long long hashCode;
	// stores wordPair
	void * wordPair;
	// stores next node in linked list
	struct _node * next;
	// stores occurences of given wordpair
	int count;
} Node;

// Returns hashtable pointer. used to create a hashtable, provide argument of 0 for the default intial length,
// or provide a custom intial length
Node ** initHT(int length);
// Returns 1 if successful, used to add a node to the hash table, returns a 1 if successful
int addHT(Node ** table, void * wordPair);
// Returns pointer to hashtable. determines whether table should be grown or not and grows it if it determines it should be
Node ** growHT(Node ** oldTable);
// frees each element in the hash table
void freeHT(Node ** table);
// Returns pointer to array. converts the hash table to an array
Node ** convertArrayHT(Node ** table);
// Returns total number of unique words in the hash table
int totalCountHT(Node ** table);
// Returns 0, 1, or -1. Comparator that helps qsort determine how to sort table
int cmpnode(const void *pa, const void *pb);
// print contents of array
void printArray(Node ** array, Node ** table, int wordCount);
// Returns sentinel node of linked List. Intializes a linked list
Node *initLL();
// Returns 1 if successful. Adds an element to a linked list
int addLL(Node * sent, void * wordPair, unsigned long long hashCode, int count);
// frees each element in linked list
void freeLL(Node * sent);
