/*
 * Name: Nathan Spelts
 * 
 * Class: CS360 - Systems Programming
 * 
 * Professor: Dr. Ben McCamish
 * 
 * Description: A program written in C targeted at the Linux platform which reads words
 * from one or more files, and prints out a list of the most frequently occurring sequential
 * pairs of words and the number of times they occurred, in decreasing order of occurrence.
 */


#include "getWord.h"
#include "hashTable.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

void errorMsg();

int main (int argc, char * argv[]) {
	Node ** table = initHT(0); // initialize the hashTable, provide argument zero for default initial array length
	
	// variable declarations
	FILE * fd;
	int wordCount = 0, factor;
	double magnitude = 0;

	// checks to see if the user provided command line arguments
	if (argc < 2) {
		// the user didn't provide any command line arguments, so we tell them how to use the program
		fprintf(stderr, "Error: no fileName arguments specified.\n\n");
		errorMsg();
		return -1;
	}

	// for loop to loop through different files to ingest
	for (int i = 1; i < argc; i++) {
		// checks to see if the first character of the first command line argument is a dash,
		// if it is we interpret it as the number of lines to show
		if (argv[1] == argv[i] && argv[1][0] == '-') {
			// checks to confirm that second character after the dash is a number
			if (58 < argv[1][1] || 47 > argv[1][1] || '\0' == argv[1][1]) {
				// if there's no number after the dash, we print an error message
				fprintf(stderr, "Error: Illegal line count argument '%s'\n\n", argv[1]);
				return 0;
			}
			// loops through string of second command line argument
			for (int j = 1; argv[1][j] != '\0'; j++) {
				// checks to confirm that the current character is a number
				if (58 < argv[1][j] || 47 > argv[1][j]) {
					// if it isn't, we're done
					break;
				}
				// we increase the magnitude to track how big the number is supposed to be
				magnitude++;
			}

			// loops through string of second command line argument
			for (int k = 1; argv[1][k] != '\0'; k++) {
				// checks to confirm that the current character is a number
				if (58 < argv[1][k] || 47 > argv[1][k]) {
					// if it isn't, we're done
					break;
				} else {
					// initialize to 1 so we can mutiply to the magnitude of the current digit
					factor = 1;
					// multiplies factor by 10 for how big the magnitude should be
					for (int x = 1; x < magnitude; x++) factor *= 10;
					// adds number to wordCount after multiplying to to give it the correct magnitude
					wordCount += ((argv[1][k] - 48) * factor);
					// decrements magnitude for the next digit
					magnitude--;
				}
			}
			// increased i for the next command line argument, which should be a file
			i++;
		}
		// open the ith file for reading
		fd = fopen(argv[i], "r");

		// edge case where we can't open the file for some reason
		if (fd == NULL) {
			fprintf(stderr, "Error: Can't read file '%s'\n", argv[i]);
			return 1;
		}
		// checks if table needs to be grown, and grows it if it does
		table = growHT(table);

		// variable declarations
		char * word = NULL;
		char * lastWord = NULL;

		while(1) {
			// fetches next word from file and assigns it to word
			word = getNextWord(fd);
			// if we're at the EOF, word is NULL so we break
			if (word == NULL) break;

			// if lastWord is null, then we're on the first word of the file
			// so we loop around again so we have a word pair
			if (lastWord != NULL) {
				// allocate enough space for both words
				char * pair = malloc(strlen(word) + strlen(lastWord) + 2);
				// copy the last word onto pair
				strcpy(pair, lastWord);
				// add a space on
				strcat(pair, " ");
				// add current word onto string
				strcat(pair, word);
				// add string to the hash table
				addHT(table, pair);
				// free string
				free(pair);
			}
			// free lastWord
			free(lastWord);
			// assign current word to last word for next loop
			lastWord = word;
		}
		// free lastword one last time
		free(lastWord);

		// close file we read from
		fclose(fd);
	}
	// print array made from hashtable
	printArray(convertArrayHT(table), table, wordCount);
	// free hash table
	freeHT(table);
	// we're done :)
	return 1;
}

// prints boiler plate usage message
void errorMsg() {
	fprintf(stderr, "\tUsage: ./wordpair <-count> fileName1 <fileName2> ...\n\n");
	fprintf(stderr, "\tWhere: count is the number of words to display.\n\n");
}
