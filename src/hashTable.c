#include "crc64.h"
#include "hashTable.h"
#include <stdlib.h>
#include <stdio.h>

// intial size of the hash table
#define LENGTH 64
// threshold for avg number of collisions, if we pass this
// value we grow the table
#define THRESHOLD 10
// growth rate of the table, everytime we grow the table
// we multiply it's size by this
#define GROWTHRATE 4
// intializes a hashtable full of sentinel nodes at the provided length,
// if the provided length is zero, we use the length declared above as the
// initial length
Node ** initHT(int length) {
	// if provided length is 0, we use the macro value defined above
	if (length == 0) length = LENGTH;
	// allocate space for the entire array
	Node ** table = malloc((length + 1) * sizeof(Node *));
	// loop through hash table and intialize a sentinel node
	// in each bucket
	for (int i = 0; i <= length; i++) {
		table[i] = initLL();
	}
	// I reserved the count variable of the 0th element of the hash table
	// to store the length. I did this because I heard global variables were
	// bad practice, and this is the best way I could find to keep the length
	// out of main.
	table[0]->count = length;
	// returns newly created table
	return table;
}

// calculates the hash of a string, and stores it in the hash table based on
// that hash key
int addHT(Node ** table, void * wordPair) {
	// recieve length from the 0th element of the hash table
	int length = table[0]->count;
	// run the hash function on a word pair to get a hash code
	unsigned long long hashCode = crc64((char *) wordPair);
	// mod the hash code by the length of the hash table
	// so we can use it as an index
	int hashIndex = (hashCode % length);
	// increase the hash index so the 0th element is still
	// reserved for storing the length of the table
	hashIndex++;
	// grab sentinel of linked list at this hash index
	Node * sent = table[hashIndex];
	// add the string and it's hashcode to this linked list in the hashtable
	return addLL(sent, wordPair, hashCode, 1);
}

// determine if table needs to be grown, and if it does then grow it
Node ** growHT(Node ** oldTable) {
	// intialize tbale length, and integer for the new length
	int oldLength = oldTable[0]->count, newLength;
	// if the average collisions is greater than the set threshold
	if ((totalCountHT(oldTable) / oldLength) > THRESHOLD) {
		// then we compute the new length, and grow the table
		newLength = oldLength * GROWTHRATE;
	} else {
		// otherwise we shouldn't grow the table, so we return the old one
		return oldTable;
	}
	// intialize new hashtable with new size
	Node ** newTable = initHT(newLength);
	// intialize variables for traversing linked list
	Node * sent;
	Node * current;
	int hashIndex;
	// loop through old hash table
	for (int i = 1; i <= oldLength; i++) {
		sent = oldTable[i];
		current = oldTable[i]->next;
		while (current != sent) {
			// calculate new hashIndex for this node, so we can put it in the new table
			hashIndex = current->hashCode % newLength;
			// increment hash index to preserve the 0th slot
			hashIndex++;
			// move this node into the new hash table
			addLL(newTable[hashIndex], current->wordPair, current->hashCode, current->count);
			current = current->next;
		}
	}
	// free old table, we're done with it
	freeHT(oldTable);
	// return new table (shiny)
	return newTable;
}

// function for freeing every node of every linked list of a hash table
void freeHT(Node ** table) {
	// retrieve length for 0th element of the hash table
	int length = table[0]->count;
	// loop through every bucket of the hash table
	for (int i = 0; i <= length; i++) {
		// free linked list at this bucket
		freeLL(table[i]);
	}
	// free memory allocated to the table itself
	free(table);
}

// move all nodes into an array in order to use qsort
Node ** convertArrayHT(Node ** table) {	
	// intialize variables
	int length = table[0]->count;
	int count = 0;
	int arrayLength = totalCountHT(table);
	// allocate space for all words in array
	Node ** array = malloc(arrayLength * sizeof(Node *));
	// loop through each linked list of hashtable
	for (int i = 1; i <= length; i++) {
		Node * sent = table[i];
		Node * current = table[i]->next;
		while (current != sent) {
			// assign current node to the array slot at this index
			array[count] = current;
			// increment index
			count++;
			current = current->next;
		}
	}
	// sort array by occurence
	qsort(array, arrayLength, sizeof(Node *), cmpnode);
	// return array
	return array;
}

// comparator function for qsort
int cmpnode(const void *pa, const void *pb) {
	// cast pa to Node * and pb to Node *
	Node * p1 = *(Node **)pa;
	Node * p2 = *(Node **)pb;
	// compare occurences of p1 and p2
	if (p1->count < p2->count) return 1;
	if (p1->count > p2->count) return -1;
	return 0;
}

// counts total number of unique words in hash table
int totalCountHT(Node ** table) {
	// retrieves length from 0th value of the hash table
	int length = table[0]->count;
	// intialize counter
	int count = 0;
	// loop through hash table and sum count element from each sentinel node
	for (int i = 1; i <= length; i++) {
		count += table[i]->count;
	}
	// return total number of unique words
	return count;
}

// function that loops through array and prints each string
void printArray(Node ** array, Node ** table, int wordCount) {
	// if a wordCount isn't provided, it will be 0
	if (wordCount == 0) {
		// loop through entire array and print stored strings
		for (int i = 0; i < totalCountHT(table); i++) {
			printf("%10d %s\n", array[i]->count, (char *) array[i]->wordPair);
		}
	// a wordCount was provided, so we'll only print as strings as was asked for or all of them
	} else {
		// if wordCount is more than the total size of the table
		if (wordCount > totalCountHT(table)) {
			// set wordCount to max size of table
			wordCount = totalCountHT(table);
		}
		// loops through length of wordCount and prints each string
		for (int i = 0; i < wordCount; i++) {
			printf("%10d %s\n", array[i]->count, (char *) array[i]->wordPair);
		}
	}
	//free array
	free(array);
}
