#include "hashTable.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// allocates space for, initializes the values of, and returns a sentinel node
Node * initLL() {
	// allocate space for the sentinel
	Node * sent = malloc(sizeof(Node));
	// initialize values of the struct
	sent->next = sent;
	sent->wordPair = "";
	sent->hashCode = 0;
	sent->count = 0;
	// return the sentinel
	return sent;
}

// adds a string to the linked list, increments the count of a node if an identical string
// is added
int addLL(Node * sent, void * wordPair, unsigned long long hashCode, int count) {
	// allocate space for new node
	Node * newNode = malloc(sizeof(Node));
	Node * current = sent;
	// loops through linked list until current is the node before the end
	while (current->next != sent) {
		current = current->next;
		// if the current nodes string is identical to the one being added, then
		// we don't need to add it again
		if (!strcmp(current->wordPair, wordPair)) {
			// we increment the count of the current node
			current->count++;
			// free the newNode since we won't need it
			free(newNode);
			// return 1 because we were successful
			return 1;
		}
	}
	// sets the current node to point to the newNode instead of the sentinel
	current->next = newNode;
	// sets the new node to point to the sentinel node instead of the current node
	newNode->next = sent;
	// stores count in the newNode (if we're resizing the hashtable this might not be 1) 
	newNode->count = count;
	// allocate space for the wordpair in the newNode
	newNode->wordPair = malloc(strlen((char *) wordPair) + 2);
	// copy the wordpair into the newNode's word pair space
	strcpy(newNode->wordPair, (char *) wordPair);
	// store hashcode of string in node (allows for quicker rehashing)
	newNode->hashCode = hashCode;
	// increment the count of the sentinel (this allows us to count total unique words quicker)
	sent->count++;
	// return 1 since we successfully added a node
	return 1;
}

void freeLL(Node * sent) {
	// intializes current to the address sentinel points to
	Node *current = sent->next;
	// intializes previous to the address of the sentinel
	Node *previous = sent;
	while (1) {
		if (previous == current) {
			free(previous);
			return;
		}
		// frees node behind current
		if(*(char *) previous->wordPair != '\0') free(previous->wordPair);
		free(previous);
		// assigns previous to address of current
		previous = current;
		// sets current to next node
		current = current->next;
		// checks to see if we're at the end of the list
		if (current == sent) {
			// if we are free final node
			free(previous->wordPair);
			free(previous);
			// exit loop
			break;
		}
		// loop on
	}
}

