## CS360 - Assignment 1 - Word Pair Counter

## Description
A program written in C targeted at the Linux platform which reads words from one or more files, and prints out a list of the most frequently occurring sequential pairs of words and the number of times they occurred, in decreasing order of occurrence. I wrote this program as a programming assignment.

# Installation
1. Clone this repository.
2. Run the make command.
3. Execute the program using the following syntax:

wordpairs <-count> fileName1 \<fileName2\> \<fileName3\> ...

## Support
If you need help running this program, shoot me an email at nate@nathanspelts.com.

## Acknowledgment
Thanks to Ben McCamish for his instructions on building this project.
